//Global variables
const bookmarksResult = document.querySelector('#bookmarksResult');
const ourForm = document.querySelector('#myForm');
let btnSubmit = document.querySelector('#submitBtn')
let expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
let regex = new RegExp(expression);

ourForm.addEventListener('submit', saveBookmark)


function saveBookmark(e){
    
    //get form value
    let siteName = document.querySelector('#siteName').value;
    let siteUrl = document.querySelector('#siteURL').value;
    
    if(!validateForm(siteName,siteUrl)){
        return false;
    };
    
    let bookmark= {
        name:siteName,
        url:siteUrl
    }
    
    
    if(localStorage.getItem('bookmarks') === null){
        //  set array
        let bookmarks = [];
        //  set to array
        bookmarks.push(bookmark);
        // set to localStorage
        localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
        
        //JSON.stringify(bookmark) ~ bierze to nasza tablice bookmarks w ktorej znajduje sie obiekt bookmark JSON i //zamienia to w stringa przed tym jak zapiszemy to w localStorage
        
    }else{
        //get bookmarks from localStoreg
        let bookmarks = JSON.parse(localStorage.getItem('bookmarks'));
        //JSON.parse() parsuje stringa do jsona
        
        //add bookmark to array
        bookmarks.push(bookmark);
        // Re-set back to localstorege ` ponowne ustawnie do LocalStorage jako string
        localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
        
        
        
    }
    ourForm.reset(); // reset the form (all inputs are clean)

    fetchBookmarks()
}

function deleteBookmark(url){
    
    let bookmarks = JSON.parse(localStorage.getItem('bookmarks'));
    for(i=0; i<bookmarks.length; i++){
        if(bookmarks[i].url == url){
            // remove from array
            bookmarks.splice(i,1);
            
        }
    }
    // Re-set back to localstorege
    localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
    
    
    fetchBookmarks()
    
}

function fetchBookmarks(){
    // Get bookmarks from localStorage
    let bookmarks = JSON.parse(localStorage.getItem('bookmarks'));
    // Get output id
    
    
    // Build output
    bookmarksResult.innerHTML = '';
    for( i = 0; i < bookmarks.length; i++){
        let name = bookmarks[i].name;
        let url = bookmarks[i].url;
        
        bookmarksResult.innerHTML += `<div class="well">
        <h3>${name}
        <a class="btn btn-default" target="_blank" href='${url}'>Visit</a> 
        <a onclick="deleteBookmark('${url}')" class="btn btn-danger" href="#">Delete</a>
        </h3>
        </div>`
    }
}


function dele(){
    let bookmarks = JSON.parse(localStorage.getItem('bookmarks'));
    
    for(i=0; i<bookmarks.length;i++){
        if(bookmarks[i].url == url){
            bookmarks.splice(i,1)
        }
    }
}


function validateForm(siteName,siteUrl){
    
    
    
    if(!siteName || !siteUrl.match(regex)){
        alert('Please correct fill the form')
        return false;
    }
    return true;
    
}





 /*   HOW WORKS LOCAL STORAGE ~ LOKALNY MAGAZYN
    
    localStorage.setItem('test', 'Hello World') ~ tworzymy item pod  pod nazwa 'test' a w tym 'test' znajduje sie napis 'hello world'
    
    
    localStorage.getItem('test'); ~ pobieramy elelment 'test' i mozemy go pokazac np w konsoli
    
    
    localStorage.removeItem('test')~usuwamy element test
    
    */